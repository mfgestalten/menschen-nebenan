/* Needed gulp config */
var gulp = require('gulp');  
var minifycss = require('gulp-minify-css');
var rename = require('gulp-rename');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var prettify = require('gulp-prettify');
var autoprefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');

/* Script task */ 
gulp.task('scripts', function() {
  return gulp.src('src/assets/js/**/*.js')
	.pipe(uglify())
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('dist/assets/js'));
});

/* CSS task */
gulp.task('css', function () {  
	gulp.src('src/assets/css/*.css')
	.pipe(autoprefixer({
		browsers: ['> 5% in DE'],
		cascade: false
	}))
	.pipe(rename({suffix: '.min'}))
	.pipe(minifycss())
	.pipe(gulp.dest('dist/assets/css'))
	.pipe(reload({stream:true}));
});

/* HTML task */
gulp.task('prettify', function() {
  gulp.src('src/*.html')
    .pipe(prettify({
	indent_size: 2,
    	brace_style: "collapse",
    }))
    .pipe(gulp.dest('dist'))
});

/* Reload task */
gulp.task('bs-reload', function () {
    browserSync.reload();
});

/* Prepare Browser-sync for localhost */
gulp.task('browser-sync', function() {
    browserSync.init(['dist/*.html', 'dist/assets/css/**/*.css', 'dist/assets/js/**/*.js'], {
        proxy: '192.168.1.112/menschen-nebenan/dist'
    });
});

/* Watch scss, js and html files, doing different things with each. */
gulp.task('default', ['css', 'browser-sync'], function () {
    /* Watch scss, run the css task on change. */
    gulp.watch(['src/assets/css/**/*.css'], ['css'])
    /* Watch app.js file, run the scripts task on change. */
    gulp.watch(['src/assets/js/**/*.js'], ['scripts'])
    /* Watch .html files, run the bs-reload task on change. */
    gulp.watch(['src/*.html'], ['prettify', 'bs-reload']);
});
