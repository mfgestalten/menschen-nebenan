// viewport fuer dynamische Darstellung speichern
var viewportWidth = $(window).width();
var viewportHeight = $(window).height();

$( document ).ready(function() {
    console.log( "ready!" );
    var plopp = $("a").offset();
    console.log(plopp);
    $('#nav-pointer-bicycle').css('margin-left', plopp.left);
    $('#nav-pointer-bicycle').show();
});

// Einstellungen fuer horizontales Scrolling via jInvertScroll jQuery-Plugin
(function($) {
	var elem = $.jInvertScroll(['.scroll'], {
		height: 10000,
		width: 8000,
		onScroll: function(percent) {
			console.log(percent);
		}
	});
	$(window).resize(function() {
		if ($(window).width() <= 768) {
			elem.destroy();
		} else {
			elem.reinitialize();
		}
	});
}(jQuery));

// Smooth Scroll und Animationseinstellungen
$('nav ul li a').on('click', function(e) {
	e.preventDefault();
    var anchor_position = $(this).offset();
    var anchor_width = $(this).width();
    var pointer_width = $('#nav-pointer-bicycle').width();
    var center_on_anchor = anchor_position.left + (anchor_width - pointer_width ) / 2;
	var top = $($(this).attr('href')).position().top;
    console.log(center_on_anchor);
    $('#nav-pointer-bicycle').css('margin-left', center_on_anchor);
	$('html, body').animate({scrollTop: top+'px'}, 1200);
});

// Auswahl-Style fuer Icons
$('.icon-link').on('click', function() {
    $(this).toggleClass('icon-link-selected');
});

// Kontrolle des Overlays
$('.open-close-form').on('click', function(e) {
    e.preventDefault();
    $('#overlay').fadeToggle();
});
